<?php namespace Entopancore\Buildform\Models;

use Model;
use October\Rain\Database\Traits\NestedTree;
use System\Traits\ViewMaker;
use Twig;
use Db;

/**
 * Model
 */
class FormField extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use ViewMaker;

    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'placeholder'];

    use NestedTree;
    /*
     * Validation
     */
    public $rules = [
        'field' => 'required',
        'title' => 'max:255',
        'name' => 'required|max:255',
        'default' => 'max:255',
        'validation' => 'max:255',
        'custom_attributes' => 'max:255',
        'class' => 'max:255',
        'placeholder' => 'max:255',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    protected $jsonable = ['options'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_buildform_forms_fields';

    public $belongsTo = [
        'forms' => ['Entopancore\BuildForm\Models\Form'],
        'field' => ['Entopancore\BuildForm\Models\Field']
    ];


    public function getHtmlAttribute()
    {

        $data = [
            'label' => $this->title,
            'is_visible_label' => $this->is_visible_label,
            'name' => $this->name,
            'default' => $this->default,
            'class' => $this->class,
            'class_input' => $this->class_input,
            'placeholder' => $this->placeholder,
            'options' => $this->options,
            'custom_attributes' => $this->custom_attributes,
            'uploadSingleFile'=>'{{ uploadSingleFile|raw }}',
            'uploadMultiFile'=>'{{ uploadMultiFile|raw }}',
            'uploadSingleImage'=>'{{ uploadSingleImage|raw }}',
            'uploadMultiImage'=>'{{ uploadMultiImage|raw }}'
        ];

        $layout = Twig::parse($this->field->markup, $data);


        return $layout;
    }


}