<?php namespace Entopancore\Buildform\Models;

use Entopancore\Utility\Traits\Modelable;
use Model;

/**
 * Model
 */
class Form extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Modelable;

    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];
    public $appends = ["html"];

    public $translatable = ['submit_label', 'success_message', 'error_message', 'subject', 'layout_email', 'layout_user_email'];

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required|max:255',
        'slug' => 'required|unique:entopancore_buildform_forms|max:100',
        'to_email' => 'email|max:100',
        'to_name' => 'max:255',
        'from_email' => 'email|max:100',
        'bcc_email' => 'email|max:100',
        'from_name' => 'max:255'
    ];
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_buildform_forms';


    public $hasMany = [
        'forms_fields' => ['Entopancore\BuildForm\Models\FormField'],
        'forms_requests' => ['Entopancore\BuildForm\Models\FormRequest'],
        'evaluation' => ['Entopanlan\Evaluation\Models\Evaluation']
    ];



    public function getHtmlAttribute()
    {
        $array = array();
        foreach ($this->forms_fields as $f) {
            $array[$f->name] = $f->html;
        }

        return \Twig::parse($this->layout_form, $array);
    }




}