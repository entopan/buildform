<?php namespace Entopancore\Buildform\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Model
 */
class Field extends Model
{
    use Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_buildform_fields';
}