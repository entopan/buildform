<?php namespace Entopancore\Buildform\Models;

use Model;
use October\Rain\Support\Collection;
use Entopancore\Buildform\Models\Form;

class FormRequestExport extends \Backend\Models\ExportModel
{
    protected $fillable = ['choose_form'];

    public function exportData($columns, $sessionKey = null)
    {
        $form = Form::with("forms_fields", "forms_requests")->where('slug', '=', $this->choose_form)->first();
        $columns = $form->forms_fields->lists("label");

        $result = new Collection();

        if ($form->forms_requests->count() > 0) {
            $forms_fields = $form->forms_fields;
            $forms_requests = $form->forms_requests;

            $result->push(["value" => implode("~", $columns)]);
            $forms_requests->each(function ($item) use ($forms_fields, $result) {
                $var = array();
                foreach ($item->forms_values as $k => $v) {
                    if (is_array($v->value))
{
                        array_push($var, implode("-", $v->value));
}
                    else
{
                        array_push($var, $v->value);
                    //$var[] = ;
}
                }
                $result->push(["value" => implode("~", $var)]);
            });

        }

        $result= $result->toArray();
foreach($result as $k=>$r)
{
foreach($r as $x=>$v)
{
$result[$k][$x]= mb_convert_encoding($v,'utf-16','utf-8');
}
}
return $result;

    }


    public function getChooseFormOptions()
    {
        return Form::lists("title", "slug");
    }


}