<?php namespace Entopancore\BuildForm\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * FormValue Model
 */
class FormValue extends Model
{

    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_buildform_request_values';

    public $rules = [

    ];
    

    public $belongsTo = [
        'forms_fields' => ['Entopancore\BuildForm\Models\FormField','key'=>'form_field_id']
    ];

    public $attachOne = [
        'upload' => 'System\Models\File'
    ];

}