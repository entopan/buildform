<?php namespace Entopancore\Buildform\Models;

use Model;

/**
 * Model
 */
class FormRequest extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];
    public $appends = ["email_admin", "email_user"];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_buildform_forms_request';


    public $belongsTo = [
        'form' => ['Entopancore\BuildForm\Models\Form'],
        //'contact' => ['Entopancore\Subscribers\Models\Contact']
    ];

    public $hasMany = [
        'forms_values' => ['Entopancore\BuildForm\Models\FormValue', 'key' => 'form_request_id']
    ];


    public $belongsToMany = [
        'forms_fields' =>
            [
                'Entopancore\BuildForm\Models\FormField',
                'table' => 'entopancore_buildform_request_values',
                'key' => 'form_request_id',
                'otherKey' => 'form_field_id',
                'timestamps' => true,
                'pivot' => ['value']
            ]
    ];

    public $attachMany = [
        'files' => 'System\Models\File',
        'images' => 'System\Models\File',
    ];
    public $attachOne = [
        'file' => 'System\Models\File',
        'image' => 'System\Models\File',
    ];

    public function getInfoAttribute()
    {
        $return = array();
        if ($forms = $this->forms_fields) {
            foreach ($forms as $row) {
                if ($row->name == "name" or $row->name == "email") {
                    array_push($return, json_decode($row->pivot->value));
                }
            }

        }
        return implode(" - ", $return);
    }

    public function getEmailAdminAttribute()
    {
        if ($form = Form::with("forms_fields")->find($this->form_id)) {
            $form = $form->toArray();
            if ($form["active_email"]) {
                $formFields = collect($form["forms_fields"]);
                $arr = [];
                foreach ($this->forms_values as $field) {
                    $arr[$formFields->where("id", $field["form_field_id"])->first()["name"]] = $field["value"];
                }
                return \Twig::parse($form["layout_email"], $arr);
            }
        }
        return false;
    }

    public function getEmailUserAttribute()
    {
        if ($form = Form::with("forms_fields")->find($this->form_id)) {
            $form = $form->toArray();
            if ($form["send_user_email"]) {
                $formFields = collect($form["forms_fields"]);
                $arr = [];
                foreach ($this->forms_values as $field) {
                    $arr[$formFields->where("id", $field["form_field_id"])->first()["name"]] = $field["value"];
                }
                return \Twig::parse($form["layout_user_email"], $arr);
            }
        }
        return false;
    }

}