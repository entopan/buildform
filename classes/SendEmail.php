<?php namespace Entopancore\Buildform\Classes;

use Mail;

class SendEmail
{
    public function sendEmailMessage($form, $data)
    {
        // SET ADMIN EMAIL
        $adminEmail = array_filter(explode(",", $form["admin_email"]));

        foreach ($adminEmail as $k => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                unset($adminEmail[$k]);
            }
        }
        if ($adminEmail) {
            $template = $this->generateTemplate($form["layout_email"], $data);
            Mail::queue('entopancore.buildform::mail.default', ["layout" => $template], function ($message) use ($form, $adminEmail) {
                if (!empty($form["from_email"])) {
                    $message->from($form["from_email"], $form["from_name"]);
                }
                $message->subject($form["subject"]);
                $message->to($adminEmail);
            });
        }

        if ($form["send_user_email"]) {
            if ($email = $this->getEmailFromUserResponse($data)) {
                $template = $this->generateTemplate($form["layout_user_email"], $data);
                Mail::queue('entopancore.buildform::mail.default', ["layout" => $template], function ($message) use ($form, $email) {
                    $message->subject($form["subject"]);
                    $message->to($email);
                });
            }
        }
        return true;

    }

    private function generateTemplate($layout, $data)
    {
        $result = [];
        foreach ($data["forms_fields"] as $ff) {
            foreach ($data["forms_values"] as $fv) {
                if ($ff["id"] == $fv["form_field_id"]) {
                    $result[$ff["name"]] = $fv["value"];
                }
            }
        }
        return \Twig::parse($layout, $result);
    }

    private function getEmailFromUserResponse($data)
    {
        foreach ($data["forms_fields"] as $ff) {
            foreach ($data["forms_values"] as $fv) {
                if ($ff["id"] == $fv["form_field_id"]) {
                    if ($ff["name"] == "email") {
                        return $fv["value"];
                    }
                }

            }
        }
        return null;

    }
}