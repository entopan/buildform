<?php namespace Entopancore\Buildform\Updates;

use Carbon\Carbon;
use Seeder;
use Entopancore\BuildForm\Models\Field;
use File;

/**
 * Class SeedFieldsTable
 * @package Renatio\FormBuilder\Updates
 */
class SeedFieldsTable extends Seeder
{

    /**
     * Seed fields
     */
    public function run()
    {
        Field::insert([
            [
                'name' => 'Text',
                'slug' => 'text',
                'markup' => File::get(__DIR__ . '/fields/_text.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Textarea',
                'slug' => 'textarea',
                'markup' => File::get(__DIR__ . '/fields/_textarea.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Dropdown',
                'slug' => 'dropdown',
                'markup' => File::get(__DIR__ . '/fields/_dropdown.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Checkbox',
                'slug' => 'checkbox',
                'markup' => File::get(__DIR__ . '/fields/_checkbox.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Checkbox List',
                'slug' => 'checkbox_list',
                'markup' => File::get(__DIR__ . '/fields/_checkbox_list.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Radio List',
                'slug' => 'radio_list',
                'markup' => File::get(__DIR__ . '/fields/_radio_list.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Number',
                'slug' => 'number',
                'markup' => File::get(__DIR__ . '/fields/_number.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Ranking',
                'slug' => 'ranking',
                'markup' => File::get(__DIR__ . '/fields/_ranking.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Star',
                'slug' => 'star',
                'markup' => File::get(__DIR__ . '/fields/_star.htm'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}