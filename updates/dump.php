<?php namespace Entopancore\Buildform\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Dump extends Migration
{
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $file = \File::get(plugins_path() . "/entopancore/buildform/updates/versions/v1.sql");
        \DB::unprepared($file);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('entopancore_buildform_fields');
        Schema::dropIfExists('entopancore_buildform_forms');
        Schema::dropIfExists('entopancore_buildform_forms_fields');
        Schema::dropIfExists('entopancore_buildform_forms_request');
        Schema::dropIfExists('entopancore_buildform_request_values');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}