-- Create syntax for TABLE 'entopancore_buildform_fields'
CREATE TABLE `entopancore_buildform_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `slug` text COLLATE utf8_unicode_ci,
  `markup` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create syntax for TABLE 'entopancore_buildform_forms'
CREATE TABLE `entopancore_buildform_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `submit_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `submit_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `success_message` text COLLATE utf8_unicode_ci,
  `error_message` text COLLATE utf8_unicode_ci,
  `active_email` tinyint(1) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `layout_email` text COLLATE utf8_unicode_ci,
  `layout_user_email` text COLLATE utf8_unicode_ci,
  `layout_form` text COLLATE utf8_unicode_ci,
  `from_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `from_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `admin_email` text COLLATE utf8_unicode_ci,
  `send_user_email` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create syntax for TABLE 'entopancore_buildform_forms_fields'
CREATE TABLE `entopancore_buildform_forms_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned DEFAULT NULL,
  `form_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_attributes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `placeholder` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_input` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_visible_label` tinyint(1) DEFAULT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `field_form_foreign` (`field_id`),
  KEY `form_field_foreign` (`form_id`),
  CONSTRAINT `field_form_foreign` FOREIGN KEY (`field_id`) REFERENCES `entopancore_buildform_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_field_foreign` FOREIGN KEY (`form_id`) REFERENCES `entopancore_buildform_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create syntax for TABLE 'entopancore_buildform_forms_request'
CREATE TABLE `entopancore_buildform_forms_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` int(10) unsigned DEFAULT NULL,
  `form_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_contact_foreign` (`form_id`),
  CONSTRAINT `form_contact_foreign` FOREIGN KEY (`form_id`) REFERENCES `entopancore_buildform_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create syntax for TABLE 'entopancore_buildform_request_values'
CREATE TABLE `entopancore_buildform_request_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_request_id` int(10) unsigned NOT NULL,
  `form_field_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formrequest_foreign` (`form_request_id`),
  KEY `formfield_foreign` (`form_field_id`),
  CONSTRAINT `formfield_foreign` FOREIGN KEY (`form_field_id`) REFERENCES `entopancore_buildform_forms_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `formrequest_foreign` FOREIGN KEY (`form_request_id`) REFERENCES `entopancore_buildform_forms_request` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;