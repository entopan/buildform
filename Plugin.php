<?php namespace Entopancore\Buildform;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{


    public function registerSettings()
    {
    }

    public function boot()
    {
        \App::register('Entopancore\Buildform\Http\BuildformServiceProvider');
    }


}
