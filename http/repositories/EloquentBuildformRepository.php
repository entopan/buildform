<?php

namespace Entopancore\Buildform\Http\Repositories;

use Entopancore\Buildform\Models\Form;
use Entopancore\Buildform\Models\FormRequest;
use Entopancore\BuildForm\Models\FormValue;

class EloquentBuildformRepository implements BuildformRepositoryInterface
{


    public function getForm($id)
    {
        return Form::with("forms_fields")->find($id);
    }

    public function getFormFields($id)
    {
        if ($form = Form::with(["forms_fields" => function ($query) {
            $query->with("field");
        }])->find($id)) {
            return $form["forms_fields"];
        }
    }

    public function getRequest($id)
    {
        return FormRequest::with(["forms_values" => function ($query) {
            $query->with("forms_fields");
        }])->find($id);
    }

    public function saveRequest($form, $data)
    {
        if ($formRequestBeforeSaveValues = $this->createFormRequest($form["id"])) {
            if ($formRequestAfterSaveValues = $this->fillForm($form, $data, $formRequestBeforeSaveValues)) {
                return $formRequestAfterSaveValues;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function createFormRequest($formId)
    {
        try {
            $formRequest = new FormRequest();
            $formRequest->form_id = $formId;
            $formRequest->save();
            return $formRequest->id;
        } catch (\Exception $e) {
            return false;
        }


    }

    public function fillForm($form, $data, $requestId)
    {
        foreach ($form["forms_fields"] as $f) {
            if (isset($data[$f["name"]])) {
                $value = new FormValue();
                $value->form_request_id = $requestId;
                $value->form_field_id = $f["id"];
                if (is_array($data[$f["name"]])) {
                    $value->value = implode(",", $data[$f["name"]]);
                } else {
                    $value->value = $data[$f["name"]];
                }
                $value->save();
            }
        }
        if ($request = FormRequest::with(["forms_fields", "forms_values"])->find($requestId)) {
            return $request->toArray();
        } else {
            return false;
        }

    }


}