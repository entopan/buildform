<?php

/**
 * Created by PhpStorm.
 * User: entopancore
 * Date: 05/06/17
 * Time: 9.23
 */

namespace Entopancore\Buildform\Http\Repositories;


interface BuildformRepositoryInterface
{

    public function getForm($id);

    public function getFormFields($id);

    public function getRequest($id);

    public function saveRequest($form, $data);

}