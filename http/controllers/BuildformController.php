<?php namespace Entopancore\Buildform\Http\Controllers;

use Entopancore\Buildform\Classes\SendEmail;
use Entopancore\Buildform\Http\Repositories\BuildformRepositoryInterface;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Mail;

class BuildformController extends Controller
{
    public $request;
    public $repository;
    public $sendEmail;

    public function __construct(BuildformRepositoryInterface $repository, Request $request)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->sendEmail = new SendEmail();
    }


    public function getForm($id)
    {
        if ($form = $this->repository->getForm($id)) {
            return getSuccessResult($form);
        } else {
            return getNotFoundResult();
        }

    }

    public function getFormFields($id)
    {
        if ($form = $this->repository->getFormFields($id)) {
            return getSuccessResult($form);
        } else {
            return getNotFoundResult();
        }

    }

    public function getRequest($id)
    {
        if ($request = $this->repository->getRequest($id)) {
            return getSuccessResult($request);
        } else {
            return getNotFoundResult();
        }
    }

    public function saveRequest($formId)
    {
        $data = $this->request->input();

        if (!googleRecaptcha($data)) {
            return getCustomResult(400, null, "Captcha code not valid");
        }

        if ($form = $form = $this->repository->getForm($formId)) {
            $rules = $this->buildValidationRules($form);
            $validation = \Validator::make($data, $rules, ["required" => "Campo obbligatorio"]);
            if ($validation->fails()) {
                return getValidationResult($validation);
            } else {
                if ($request = $this->repository->saveRequest($form, $data)) {
                    if ($this->sendEmail->sendEmailMessage($form, $request)) {
                        return getSuccessResult(["message" => $form->success_message], $form->success_message);
                    } else {
                        return getErrorResult($form->error_message);
                    }

                } else {
                    return getErrorResult($form->error_message);
                }

            }
        }
        return false;

    }


    private function buildValidationRules($form)
    {
        $arr = [];
        foreach ($form["forms_fields"] as $field) {
            $arr[$field["name"]] = $field["validation"];
        }
        return $arr;
    }


}