<?php namespace Entopancore\Buildform\Http;

use Illuminate\Support\ServiceProvider;

class BuildformServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind("Entopancore\Buildform\Http\Repositories\BuildformRepositoryInterface","Entopancore\Buildform\Http\Repositories\EloquentBuildformRepository");
    }

}
