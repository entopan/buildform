<?php

Route::group(
    [
        'prefix' => 'api/v1/public/buildform',
        'middleware' => [
            'api'
        ],
    ], function () {
    Route::get('form/{id}', ['uses' => 'Entopancore\Buildform\Http\Controllers\BuildformController@getForm', 'as' => 'getForm']);
    Route::get('form/{id}/fields', ['uses' => 'Entopancore\Buildform\Http\Controllers\BuildformController@getFormFields', 'as' => 'getFormFields']);
    Route::post('form/{id}', ['uses' => 'Entopancore\Buildform\Http\Controllers\BuildformController@saveRequest', 'as' => 'saveRequest']);
    Route::get('request/{id}', ['uses' => 'Entopancore\Buildform\Http\Controllers\BuildformController@getRequest', 'as' => 'getRequest']);

});