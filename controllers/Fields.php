<?php namespace Entopancore\Buildform\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Fields extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['entopancore.buildform.superadmin'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Buildform', 'buildform', 'fields');
    }
}