<?php namespace Entopancore\Buildform\Controllers;

use Backend\Classes\Controller;
use Entopancore\Buildform\Models\FormField;
use BackendMenu;

class Forms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ReorderController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['entopancore.buildform.superadmin'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Buildform', 'buildform', 'forms');
    }

    public function reorder($formId)
    {
        $this->pageTitle = trans('entopancore.buildform::lang.reorder');

        $toolbarConfig = $this->makeConfig();
        $toolbarConfig->buttons = '$/entopancore/buildform/controllers/forms/_reorder_toolbar.htm';
        $this->vars['toolbar'] = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
        $this->vars['records'] = FormField::where('form_id', $formId)->get();
        $this->vars['formId'] = $formId;
    }

    public function reorder_onMove()
    {
        $sourceNode = FormField::find(post('sourceNode'));
        $targetNode = post('targetNode') ? FormField::find(post('targetNode')) : null;

        if ($sourceNode == $targetNode)
            return;

        switch (post('position')) {
            case 'before':
                $sourceNode->moveBefore($targetNode);
                break;
            case 'after':
                $sourceNode->moveAfter($targetNode);
                break;
            case 'child':
                $sourceNode->makeChildOf($targetNode);
                break;
            default:
                $sourceNode->makeRoot();
                break;
        }
    }

}