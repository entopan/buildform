<?php namespace Entopancore\Buildform\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Requests extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ImportExportController'
    ];
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';
    public $bodyClass = "compact-container";

    public $requiredPermissions = ['entopancore.buildform.superadmin'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Buildform', 'buildform', 'requests');
    }



}